module slurmjobs-memory-exporter

go 1.17

require (
	github.com/VictoriaMetrics/metrics v1.24.0
	gitlab.com/jfolz/justlogs v0.2.3
)

require (
	github.com/valyala/fastrand v1.1.0 // indirect
	github.com/valyala/histogram v1.2.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
