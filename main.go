package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/VictoriaMetrics/metrics"
	jl "gitlab.com/jfolz/justlogs"
)

var (
	// BuildVersion program version
	BuildVersion string = "1.2.1"
	// BuildTime time of build
	BuildTime string = "none"
	// Creater logger
	log = jl.NewLogger(os.Stdout, jl.Info)
	// Counter for parsing errors
	parseErrors = metrics.NewCounter("slurmjobs_memory_parse_errors")
	// How long requests take
	requestDuration = metrics.NewSummary("requests_duration_seconds")
	// Cgroup v2 glob pattern for job processes
	procGlobPattern = "step_*/user/task_*/cgroup.procs"
	// Cgroup v2 glob pattern for job processes
	procGlobPatternLegacy = "step_*/task_*/cgroup.procs"
	// Cgroup v2 glob pattern for metrics
	metricGlobPattern = "/sys/fs/cgroup/system.slice/slurmstepd.scope/job_*"
	// Legacy cgroup v1 glob pattern
	metricGlobPatternLegacy = "/sys/fs/cgroup/memory/slurm/uid_*/job_*"
	// Cgroup v2 metrics that will be recorded
	metricNames = []string{
		"memory.current",
		"memory.high",
		"memory.low",
		"memory.max",
		"memory.min",
		"memory.peak",
		"memory.swap.current",
	}
	// Legacy cgroup v1 metrics
	metricNamesLegacy = []string{
		"memory.failcnt",
		"memory.limit_in_bytes",
		"memory.max_usage_in_bytes",
		"memory.soft_limit_in_bytes",
		"memory.swappiness",
		"memory.usage_in_bytes",
		"memory.kmem.failcnt",
		"memory.kmem.limit_in_bytes",
		"memory.kmem.max_usage_in_bytes",
		"memory.kmem.tcp.failcnt",
		"memory.kmem.tcp.limit_in_bytes",
		"memory.kmem.tcp.max_usage_in_bytes",
		"memory.kmem.tcp.usage_in_bytes",
		"memory.kmem.usage_in_bytes",
		"memory.memsw.failcnt",
		"memory.memsw.limit_in_bytes",
		"memory.memsw.max_usage_in_bytes",
		"memory.memsw.usage_in_bytes",
	}
)

func jobHasProcs(path string) bool {
	// List all jobs for all users
	files, err := filepath.Glob(filepath.Join(path, procGlobPattern))
	if err != nil {
		log.Warn("could not check for procs").E(err).End()
		return false
	}

	for _, file := range files {
		value, _ := os.ReadFile(file)
		if len(value) > 0 {
			return true
		}
	}

	return false
}

func handleMetricsRequest(w http.ResponseWriter, req *http.Request) {
	startTime := time.Now()

	// List all jobs for all users
	files, err := filepath.Glob(metricGlobPattern)
	if err != nil {
		log.Error("Slurm memory cgroup not found").E(err).End()
		parseErrors.Inc()
		w.WriteHeader(500)
		return
	}

	// Loop over all job dirs
	for _, job := range files {
		// Skip job if there are no active processes listed
		if !jobHasProcs(job) {
			continue
		}
		// Parse jobid: /sys/fs/cgroup/memory/slurm/uid_<uid>/job_<jobid>
		jobid, err := strconv.Atoi(job[strings.LastIndex(job, "_")+1:])
		if err != nil {
			log.Error("could not parse jobid in path").E(err).S("path", job).End()
			parseErrors.Inc()
			continue
		}
		// Read metric values
		for _, metric := range metricNames {
			value, err := os.ReadFile(filepath.Join(job, metric))
			if err == nil {
				n, err := fmt.Fprintf(
					w,
					"slurmjobs_%s{jobid=\"%d\"} %s\n",
					strings.ReplaceAll(metric, ".", "_"),
					jobid,
					strings.TrimSuffix(string(value), "\n"),
				)
				if n == 0 || err != nil {
					if errors.Is(err, syscall.EPIPE) {
						return
					}
					log.Error("error writing metric line").S("name", metric).I("n", n).E(err).End()
					parseErrors.Inc()
				}
			} else if !errors.Is(err, os.ErrNotExist) {
				log.Error("could not read metric").E(err).I("jobid", jobid).S("metric", metric).End()
				parseErrors.Inc()
			}
		}
		// Read stat file values
		f, err := os.Open(filepath.Join(job, "memory.stat"))
		if err == nil {
			scanner := bufio.NewScanner(f)
			for scanner.Scan() {
				line := scanner.Text()
				parts := strings.Split(line, " ")
				if len(parts) == 2 {
					n, err := fmt.Fprintf(
						w,
						"slurmjobs_memory_stat_%s{jobid=\"%d\"} %s\n",
						parts[0],
						jobid,
						parts[1],
					)
					if n == 0 || err != nil {
						if errors.Is(err, syscall.EPIPE) {
							return
						}
						log.Error("error writing stat line").I("n", n).E(err).End()
						parseErrors.Inc()
					}
				} else {
					log.Error("wrong number of parts in stat line").I("expected", 2).I("found", len(parts)).End()
					parseErrors.Inc()
				}
			}
		} else {
			log.Error("could not read stat file").E(err).I("jobid", jobid).End()
			parseErrors.Inc()
		}
	}

	// Output process metrics etc.
	requestDuration.UpdateDuration(startTime)
	metrics.WritePrometheus(w, true)
}

func main() {
	// Parse command line arguments
	var listenAddress string
	var printVersion bool
	var legacyMetrics bool
	flag.BoolVar(&printVersion, "version", false, "print version info")
	flag.BoolVar(&legacyMetrics, "legacy", false, "return legacy cgroup v1 metrics")
	flag.StringVar(&listenAddress, "listenAddress", ":9200", "HTTP listen address")
	flag.Parse()

	// Print version info
	if printVersion {
		fmt.Printf("%s (%s)\n", BuildVersion, BuildTime)
		os.Exit(0)
	}

	if legacyMetrics {
		procGlobPattern = procGlobPatternLegacy
		metricGlobPattern = metricGlobPatternLegacy
		metricNames = metricNamesLegacy
	}

	// Expose metrics at root and `/metrics`
	http.HandleFunc("/", handleMetricsRequest)
	http.HandleFunc("/metrics", handleMetricsRequest)

	// Start server
	log.Info("start listening").S("listenAddress", listenAddress).End()
	if err := http.ListenAndServe(listenAddress, nil); err != nil {
		log.Fatal("could not start server").E(err).End()
	}
}
