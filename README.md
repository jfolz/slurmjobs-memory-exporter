# Slurm jobs memory exporter

Prometheus exporter for memory stats of Slurm jobs.
See [Linux kernel docs](https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v1/memory.html) for details on exported metrics.

**WARNING:** Every job creates roughly 50 new time series. This may make your Prometheus very sad, so use at your own risk.


## Legacy cgroup v1 support

As of version 1.2.0 the exporter defaults to the cgroup v2 paths for Slurm.
To use with legacy cgroup v1, use with `slurmjobs-memory-exporter -legacy`.


## Example output
```
slurmjobs_memory_current{jobid="691846"} 55751512064
slurmjobs_memory_high{jobid="691846"} 274877906944
slurmjobs_memory_low{jobid="691846"} 0
slurmjobs_memory_max{jobid="691846"} 274877906944
slurmjobs_memory_min{jobid="691846"} 0
slurmjobs_memory_swap_current{jobid="691846"} 0
slurmjobs_memory_stat_anon{jobid="691846"} 40748924928
slurmjobs_memory_stat_file{jobid="691846"} 14561017856
slurmjobs_memory_stat_kernel_stack{jobid="691846"} 6848512
slurmjobs_memory_stat_pagetables{jobid="691846"} 164986880
slurmjobs_memory_stat_percpu{jobid="691846"} 2312064
slurmjobs_memory_stat_sock{jobid="691846"} 53248
slurmjobs_memory_stat_shmem{jobid="691846"} 14560980992
slurmjobs_memory_stat_file_mapped{jobid="691846"} 2524905472
slurmjobs_memory_stat_file_dirty{jobid="691846"} 0
slurmjobs_memory_stat_file_writeback{jobid="691846"} 0
slurmjobs_memory_stat_swapcached{jobid="691846"} 0
slurmjobs_memory_stat_anon_thp{jobid="691846"} 0
slurmjobs_memory_stat_file_thp{jobid="691846"} 0
slurmjobs_memory_stat_shmem_thp{jobid="691846"} 0
slurmjobs_memory_stat_inactive_anon{jobid="691846"} 55254536192
slurmjobs_memory_stat_active_anon{jobid="691846"} 54931456
slurmjobs_memory_stat_inactive_file{jobid="691846"} 36864
slurmjobs_memory_stat_active_file{jobid="691846"} 0
slurmjobs_memory_stat_unevictable{jobid="691846"} 0
slurmjobs_memory_stat_slab_reclaimable{jobid="691846"} 107023136
slurmjobs_memory_stat_slab_unreclaimable{jobid="691846"} 156317576
slurmjobs_memory_stat_slab{jobid="691846"} 263340712
slurmjobs_memory_stat_workingset_refault_anon{jobid="691846"} 0
slurmjobs_memory_stat_workingset_refault_file{jobid="691846"} 0
slurmjobs_memory_stat_workingset_activate_anon{jobid="691846"} 0
slurmjobs_memory_stat_workingset_activate_file{jobid="691846"} 0
slurmjobs_memory_stat_workingset_restore_anon{jobid="691846"} 0
slurmjobs_memory_stat_workingset_restore_file{jobid="691846"} 0
slurmjobs_memory_stat_workingset_nodereclaim{jobid="691846"} 0
slurmjobs_memory_stat_pgfault{jobid="691846"} 135402848
slurmjobs_memory_stat_pgmajfault{jobid="691846"} 0
slurmjobs_memory_stat_pgrefill{jobid="691846"} 0
slurmjobs_memory_stat_pgscan{jobid="691846"} 0
slurmjobs_memory_stat_pgsteal{jobid="691846"} 0
slurmjobs_memory_stat_pgactivate{jobid="691846"} 13398
slurmjobs_memory_stat_pgdeactivate{jobid="691846"} 0
slurmjobs_memory_stat_pglazyfree{jobid="691846"} 0
slurmjobs_memory_stat_pglazyfreed{jobid="691846"} 0
slurmjobs_memory_stat_thp_fault_alloc{jobid="691846"} 0
slurmjobs_memory_stat_thp_collapse_alloc{jobid="691846"} 0
requests_duration_seconds_sum 0.001601603
requests_duration_seconds_count 1
requests_duration_seconds{quantile="0.5"} 0.001601603
requests_duration_seconds{quantile="0.9"} 0.001601603
requests_duration_seconds{quantile="0.97"} 0.001601603
requests_duration_seconds{quantile="0.99"} 0.001601603
requests_duration_seconds{quantile="1"} 0.001601603
slurmjobs_memory_parse_errors 1
go_memstats_alloc_bytes 995328
go_memstats_alloc_bytes_total 995328
go_memstats_buck_hash_sys_bytes 3876
go_memstats_frees_total 70
go_memstats_gc_cpu_fraction 0
go_memstats_gc_sys_bytes 7011064
go_memstats_heap_alloc_bytes 995328
go_memstats_heap_idle_bytes 2457600
go_memstats_heap_inuse_bytes 1441792
go_memstats_heap_objects 1548
go_memstats_heap_released_bytes 2424832
go_memstats_heap_sys_bytes 3899392
go_memstats_last_gc_time_seconds 0
go_memstats_lookups_total 0
go_memstats_mallocs_total 1618
go_memstats_mcache_inuse_bytes 86400
go_memstats_mcache_sys_bytes 93600
go_memstats_mspan_inuse_bytes 25120
go_memstats_mspan_sys_bytes 32640
go_memstats_next_gc_bytes 4194304
go_memstats_other_sys_bytes 591060
go_memstats_stack_inuse_bytes 294912
go_memstats_stack_sys_bytes 294912
go_memstats_sys_bytes 11926544
go_cgo_calls_count 0
go_cpu_count 72
go_gc_duration_seconds{quantile="0"} 0
go_gc_duration_seconds{quantile="0.25"} 0
go_gc_duration_seconds{quantile="0.5"} 0
go_gc_duration_seconds{quantile="0.75"} 0
go_gc_duration_seconds{quantile="1"} 0
go_gc_duration_seconds_sum 0
go_gc_duration_seconds_count 0
go_gc_forced_count 0
go_gomaxprocs 72
go_goroutines 4
go_threads 5
go_info{version="go1.20.4"} 1
go_info_ext{compiler="gc", GOARCH="amd64", GOOS="linux", GOROOT="/usr/local/go"} 1
process_cpu_seconds_system_total 0
process_cpu_seconds_total 0
process_cpu_seconds_user_total 0
process_major_pagefaults_total 0
process_minor_pagefaults_total 692
process_num_threads 5
process_resident_memory_bytes 5599232
process_start_time_seconds 1685014985
process_virtual_memory_bytes 731299840
process_virtual_memory_peak_bytes 731299840
process_resident_memory_peak_bytes 5599232
process_resident_memory_anon_bytes 1716224
process_resident_memory_file_bytes 3883008
process_resident_memory_shared_bytes 0
process_io_read_bytes_total 3591
process_io_written_bytes_total 4329
process_io_read_syscalls_total 32
process_io_write_syscalls_total 3
process_io_storage_read_bytes_total 0
process_io_storage_written_bytes_total 0
```
